module.exports = function interactions() {
    "use strict";

    //dependancies-----------------------------------------------
    const modManager         = require('./lib/mod-manager')();
    const shell              = require('electron').shell;
    const remote             = require('electron').remote;
    const {dialog}           = require('electron').remote;
    const fs                 = require('fs');
    const ini                = require('ini');
    const sanitize           = require("sanitize-filename");
    window.$ = window.jQuery = require('./jquery-3.1.0.min');

    //main-------------------------------------------------------
    //get the config file
    modManager.getConfig(function(config) {
        $(document).ready(function() {
            //prepare the framework and populate the mods----------------
            modManager.prepareFramework(config.gamePath, (error, manifest) => {
                modManager.populateMods();
            });

            //events-----------------------------------------------------

            //update version string
            const version = require(`${__dirname}\\..\\..\\package.json`).version;
            $('.version').html(`v${version} by Frosthaven`);

            //launch links in default browser
            $(document).on('click', 'a[href^="http"]', function(event) {
                event.preventDefault();
                shell.openExternal(this.href);
            });

            //toggle priority
            $(document).on('click', '.priority-toggle', function(event) {
                event.preventDefault();
                event.stopPropagation();

                const $el = $(this).closest('li');
                const priority = parseInt($el.attr('data-priority'));
                const name     = $el.find('h1').text();
                const ext      = $el.attr('data-ext');
                modManager.togglePriority(priority, name, ext, (state) => {
                    $el.attr('data-priority', state.priority);
                    $el.attr('data-ext', state.ext);
                    modManager.sortModList();
                    modManager.markDirty();
                });
            });

            //toggle mod on click
            $(document).on('click', 'ul.modlist li', function(event) {
                event.preventDefault();
                event.stopPropagation();

                if ($('.no-change').length <= 0) {
                    const $el     = $(this);
                    const ext     = $(this).attr('data-ext');
                    const name    = $(this).find('h1').text();

                    modManager.toggleMod(name, ext, (state) => {
                        $el.closest('li').toggleClass('enabled');
                        $el.closest('li').attr('data-ext', state.ext);
                    });

                    if (ext==="dlc"||ext==="dlcbak") {
                        modManager.markDirty();
                    }
                }
            });

            //toggle all mods on
            $(document).on('click', 'a[href="#enable-all"]', function(event) {
                event.stopPropagation();
                event.preventDefault();
                if ($('.no-change').length <= 0) {
                    if ($('.load-indicator').hasClass('loaded')) {
                        $('ul.modlist li').each(function(el) {
                            const $el = $(this);
                            if (!$el.hasClass('enabled')) {
                                $el.click();
                            }
                        });
                    }
                }
            });

            //toggle all mods off
            $(document).on('click', 'a[href="#disable-all"]', function(event) {
                event.stopPropagation();
                event.preventDefault();
                if ($('.no-change').length <= 0) {
                    if ($('.load-indicator').hasClass('loaded')) {
                        $('ul.modlist li').each(function(el) {
                            const $el = $(this);
                            if ($el.hasClass('enabled')) {
                                $el.click();
                            }
                        });
                    }
                }
            });

            //reload all mods
            $(document).on('click', 'a[href="#reload-mods"]', function(event) {
                event.stopPropagation();
                event.preventDefault();
                if ($('.no-change').length <= 0) {
                    if ($('.load-indicator').hasClass('loaded')) {
                        modManager.clearDirty();
                        modManager.populateMods();
                    }
                }
            });

            //delete mod
            $(document).on('click', 'ul.modlist li .trash', function(event) {
                event.stopPropagation();
                event.preventDefault();
                const $el = $(this).closest('li');
                const cName = $el.attr('data-name');
                const cExt = $el.attr('data-ext');
                const r = confirm(`Are you sure you want to delete \`${cName}\`?`);
                if (r == true) {
                    modManager.deleteMod(cName, cExt);
                    if (cExt==="dlc"||cExt==="dlcbak") {
                        modManager.markDirty();
                    }                    
                } else {
                    //cancel
                }
            });

            //rename mod
            $(document).on('click', 'ul.modlist li .pen', function(event) {
                event.stopPropagation();
                event.preventDefault();
                const $el = $(this).closest('li');
                const cName = $el.attr('data-name');

                if ($el.find('h1').attr('contenteditable') === 'true') {
                    $el.find('h1').attr('contenteditable','false');
                    $el.find('h1').blur();
                } else {
                    $el.find('h1').attr('contenteditable','true');
                    $el.find('h1').addClass('no-change');
                    $el.find('h1').focus();
                }
            });
            $(document).on('click', 'ul.modlist li h1[contenteditable="true"]', function(event) {
                event.stopPropagation();
            });
            $(document).on('focus', 'ul.modlist li h1[contenteditable="true"]', function(event) {
                document.execCommand('selectAll',false,null);
            });
            $(document).on('keydown', 'ul.modlist li h1[contenteditable="true"]', function(event) {
                if (event.keyCode === 13) {
                    //enter pressed
                    $(this).blur();
                    return false;
                } else if (event.keyCode ==27) {
                    //escape pressed
                    $(this).addClass('cancel');
                    $(this).blur();
                    return false;
                }
            });
            $(document).on('blur', 'ul.modlist li h1[contenteditable="true"]', function(event) {
                const toText = $(this).text();
                $(this).attr('contenteditable','false');
                const $el = $(this).closest('li');
                let alreadyExists = false;                
                $.each($(`.modlist li`), (i, el) => {
                    if ($(el).attr('data-name').toLowerCase() === toText.toLowerCase()) {
                        alreadyExists = true;
                    }
                });

                if (alreadyExists || $(this).text() === "" || $(this).hasClass('cancel')) {
                    $(this).removeClass('cancel');
                    $(this).text($el.attr('data-name'));
                } else {
                    const cName   = `${$el.attr('data-name')}`;
                    const newName = `${sanitize($(this).text())}`;
                    modManager.renameMod(cName, newName, $el.attr('data-ext'));
                    if ($el.attr('data-ext')==='dlc'||$el.attr('data-ext')==='dlcbak') {
                        modManager.markDirty();
                    }
                }
                setTimeout(() => {
                    $(this).removeClass('no-change');
                },500);
            });

            //minimize
            $(document).on('click', '.win-control .minimize', function(event) {
                const window = remote.getCurrentWindow();
                window.minimize();
            });

            //close
            $(document).on('click', '.win-control .close, a[href="#close"]', function(event) {
                const window = remote.getCurrentWindow();
                window.close();
            });

            //browse the folder
            $(document).on('click', 'a[href="#modfolder"]', function(event) {
                modManager.openFolder();
            });

            //launch game
            $(document).on('click', 'a[href="steam://rungameid/310950"]', function(event) {
                const window = remote.getCurrentWindow();
                setTimeout(function() {
                    window.close();
                }, 1000);
            });


            //drag&drop
            $(document).on('dragover', function(event) {
                event.preventDefault();
            });
            $(document).on('drop', function(event) {
                event.preventDefault();

                if ($(".load-indicator").hasClass("loaded")) {
                    const files = event.originalEvent.dataTransfer.files;
                    Object.keys(files).forEach((i) => {
                        _handleSubmittedFile(files[i].path);
                    });
                }
            });

            //open file dialogue
            const _handleSubmittedFile = (file) => {
                //need to grab the name dynamically
                const name   = file.replace(/^.*[\\\/]/, '');
                const source = file;

                if (name.endsWith('.pak')||name.endsWith('.bak')||name.endsWith('.dlc')||name.endsWith('.dlcbak')) {
                    //we can add it now
                    modManager.addMod({
                        source: source,
                        name:name
                    });
                    if (name.endsWith('dlc')||name.endsWith('dlcbak')) {
                        modManager.markDirty();
                    }
                } else {
                    const stats = fs.lstatSync(source);
                    if (stats.isDirectory()) {
                        //does it contain StreetFighterV?
                        let validStructure = false;
                        try {
                            const subfolderstats = fs.lstatSync(`${source}\\StreetFighterV`);
                            validStructure = subfolderstats.isDirectory()
                        } catch(err) {}

                        if (validStructure) {
                            let mName = false;
                            try {
                                const cfg = ini.parse(fs.readFileSync(`${source}\\modinfo.ini`, 'utf-8'));
                                mName = sanitize(cfg.name);
                            } catch(err) {}

                            if (!mName) {
                                const parts = source.split("\\");
                                mName = parts[parts.length-1];
                            }
                            const u4pak = `${process.cwd()}\\resources\\ex\\u4pak.exe`;
                            const target= `[M] ${mName}.pak`;
                            const from  = `"StreetFighterV"`;
                            const args = [`pack`, target, from];
                            const child = require('child_process').exec(`cmd.exe /c ""${u4pak}" pack "${target}" "${from}""`, {cwd: `${source}`}, function(error, stdout, stderr) {
                                if (error && typeof error === "string" && !error.endsWith("'StreetFighterV'")) {
                                    window.alert(error);
                                } else if (error) {
                                    console.log(error);
                                } else if (!error) {
                                    modManager.addMod({
                                        source: `${source}\\${target}`,
                                        name: `[MM] ${mName}.pak`
                                    });
                                }
                            });
                        } else {
                            window.alert(`${source}\n\nThis doesn't appear to be a mod folder. Valid mod folders have a StreetFighterV folder inside of them.`);
                        }
                    }
                }
            };

            const _handleOpenFile = (opts) => {
                if ($(".load-indicator").hasClass("loaded")) {
                    dialog.showOpenDialog(remote.getCurrentWindow(), opts, function (files) {
                        if (files === undefined) return;
                        files.forEach((file) => { _handleSubmittedFile(file)});
                    });
                }
            };

            $(document).on('click', 'a[href="#add-paks"]', function(event) {
                _handleOpenFile({
                    properties: [
                        'openFile',
                        'multiSelections'
                    ],
                    filters: [
                        {name: 'SFV Pak Mods', extensions: ['pak', 'bak', 'dlc', 'dlcbak'] }
                    ],
                    title:"Select Pak Files",
                    buttonLabel:"Add Selected Mods"
                });
            });

            $(document).on('click', 'a[href="#add-mods"]', function(event) {
                _handleOpenFile({
                    properties: [
                        'openDirectory',
                        'multiSelections'
                    ],
                    filters: [
                        {name: 'SFV Mods', extensions: ['*.*'] }
                    ],
                    title:"Select Mod Folders (Parent folders that have StreetFighterV in them)",
                    buttonLabel:"Import Selected Mods"
                });
            });

            //ignore clicks for disabled buttons
            $(document).on('click', '.disabled', function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            //toggle dev tools
            $(document).on('keydown', function(e) {
                if (e.which === 123) {
                    remote.getCurrentWindow().toggleDevTools();
                }
            });
        });
    });
};
