/*
    - customize the app icon
    - release for testing
*/

module.exports = function modManager() {
    "use strict";

    const lib    = {};
    const fs     = require('fs');
    const {remote} = require('electron');
    const {shell, dialog} = require('electron').remote;
    const crypto = require('crypto');
    const path = require('path');
    const sanitize = require('sanitize-filename');
    //todofix const du = require('diskusage');

    lib.dir = {
        localAppData: "",
        save: remote.app.getPath("userData"),
        game: "",
        pak: "",
        mod: "",
        meta: ""
    };

    lib.manifest = {
        defaults: {
            "major_version":      "1",
            "minor_version":      "0",
            "is_big_patch":       false,
            "slipstream_enabled": true,
            "files":              []
        },
        contents: {},
        path: () => { return `${lib.dir.localAppData}\\StreetFighterV\\Saved\\download\\dlc.manifest`},
        commit: (callback) => {
            lib.manifest.contents.slipstream_enabled = "true";
            lib.manifest.contents.is_big_patch = "false";
            lib.manifest.sort();
            lib.manifest.contents.files.forEach((file) => {
                file.slipstreamed = true;
            });
            fs.writeFileSync(lib.manifest.path(), JSON.stringify(lib.manifest.contents,null,2));
            if (typeof callback === "function") { callback(null, lib.manifest.contents); }
        },
        pull: (callback) => {
            let err = null;
            try {
                lib.manifest.contents = JSON.parse(fs.readFileSync(lib.manifest.path(), "utf8"));
            } catch(error) {
                lib.manifest.contents = lib.manifest.defaults;
                err = error;
            }

            if (typeof callback === "function") { callback(err, lib.manifest.contents); }
        },
        sort: () => {
            lib.manifest.contents.files.sort((a, b) => {
                return (a.name.localeCompare(b.name))*-1;
            });
        }
    };

    lib.markDirty = function() {
        $('a[href="#reload-mods"]').addClass('warn-reload').html('Save');
        $('.dirty-warning').addClass('warn-reload');
        $('.game-launcher').addClass('disabled').attr('href',"void(0);");
    };

    lib.clearDirty = function() {
        $('a[href="#reload-mods"]').removeClass('warn-reload').html('Reload');
        $('.dirty-warning').removeClass('warn-reload');
        $('.game-launcher').removeClass('disabled').attr('href','steam://rungameid/310950');
    };

    lib.md5 = function(filePath, callback) {
        let hash = crypto.createHash('md5');
        const stream = fs.createReadStream(filePath);
        stream.on('data', function(data) {
            hash.update(data, 'utf8');
        });
        stream.on('end', function() {
            if (typeof callback === "function") {
                const md5 = hash.digest('hex');
                callback(md5);
            }
        });
    };

    lib.openFolder = function() {
        shell.showItemInFolder(lib.dir.mod + "/*");
    };

    lib.mkdir = function(path) {
        try {
            fs.mkdirSync(path);
        } catch(e) {
            if ( e.code != 'EEXIST' ) throw e;
        }
    };

    lib.cleanOlder = function() {

    };

    lib.requestGameFolder = function(callback) {
        window.alert('Please select your StreetFighterV game folder.\nThis is usually: `/Steam/SteamApps/common/StreetFighterV`');
        const path = dialog.showOpenDialog({
            properties: ['openDirectory']
        });

        lib.verifyGameFolder(path, function(verified) {
            if (verified) {
                callback(path);
            } else {
                window.alert('This is not a StreetFighterV game folder.\nApplication will now close.')
                remote.getCurrentWindow().close();
            }
        });
    };

    lib.verifyGameFolder = function(path, callback) {
        //check for the location of the pak folder
        const pakDir = `${path}\\StreetFighterV\\Content\\Paks`;
        let verified = false;
        try {
            const stats = fs.lstatSync(pakDir);
            if (stats.isDirectory()) {
                verified = true;
            }
        } catch (e) { }
        callback(verified);
    };

    lib.getConfig = function(callback) {
        //read the config file
        let config;
        let needConfig;
        try {
            config = require(`${lib.dir.save}\\config.json`);
        } catch(err) {
            needConfig = true;
        }

        if (needConfig || !config.gamePath) {
            lib.requestGameFolder(function(path) {
                config = {
                    gamePath: path
                }
                fs.writeFileSync(`${lib.dir.save}\\config.json`, JSON.stringify(config,null,4));
            });
        } else {
            lib.verifyGameFolder(config.gamePath, function(verified) {
                if (!verified) {
                    lib.requestGameFolder(function(path) {
                        config = {
                            gamePath: path
                        }
                        fs.writeFileSync(`${lib.dir.save}\\config.json`, JSON.stringify(config,null,4));
                    });
                }
            });
        }

        callback(config);
    };

    lib.deleteMod = function(cname, cext, callback) {
        try {
            fs.unlink(`${lib.dir.mod}\\${cname}.${cext}`);
            fs.unlink(`${lib.dir.mod}\\${cname}.${cext}`);
        } catch(e) {}

        $(`.modlist li[data-name="${cname}"]`).remove();
    };

    lib.addMod = function(data, callback) {
        const source = data.source;
        const name   = data.name;
        const noIO   = data.noIO;
        if (name.toLowerCase().startsWith("zzz_modready")) { return; }
        lib.md5(source, function(hash) {
            let r;
            let w;
            const parts = name.split(".");
            const cExt  = parts[parts.length-1];
            const cName = name.slice(0, (cExt.length+1)*-1);

            //first, figure out if we have this mod installed already
            let alreadyInstalled = false;
            if ($(`.modlist li[data-md5="${hash}"]`).length > 0 || $(`.modlist li[data-name="${cName}"]`).length > 0) {
                alreadyInstalled = true;
            }

            if (alreadyInstalled && !noIO) {
                const $el = $(`.modlist li[data-md5="${hash}"]`).length > 0 ? $(`.modlist li[data-md5="${hash}"]`) : $(`.modlist li[data-name="${cName}"]`);
                window.alert(`Already Installed:\n\nSource:\n     ${cName}\n     md5: ${hash}\n\nDestination:\n     ${$el.attr('data-name')}\n     md5: ${$el.attr('data-md5')}\n\nPlease Delete the installed mod and try again if this was your intention.`);
            } else {
                if (!noIO) {
                    //copy the file to the ~mods directory
                    r = fs.createReadStream(source).pipe(w = fs.createWriteStream(`${lib.dir.mod}/${name}`));
                    w.on('finish',() => {
                        //add it to the ui
                        const priority = (cExt === "dlcbak"||cExt ==="dlc") ? 1 : 2;
                        const cEnableClass = (cExt === "dlc"||cExt ==="pak")? "enabled" : "";
                        const html = `<li data-priority="${priority}" data-md5="${hash}" data-name="${cName}" data-ext="${cExt}" class="${cEnableClass}"><h1>${cName}</h1><div class="toggle"><div class="inner"><span>on</span><span class="nob"></span><span>off</span></div></div><div class="trash"></div><div class="priority-toggle"></div><div class="pen"></div></li>`;
                        $(".modlist").append($(html));
                        lib.sortModList();

                        //send the callback
                        if (typeof callback === "function") { callback(null, hash); }
                    });
                    w.on('error',(error) => {
                        if (typeof callback === "function") { callback(error, hash); }
                    });
                } else {
                    //noIO is true, so just add to the list
                    //add it to the ui
                    const priority = (cExt === "dlcbak"||cExt ==="dlc") ? 1 : 2;
                    const cEnableClass = (cExt === "dlc"||cExt ==="pak")? "enabled" : "";
                    const html = `<li data-priority="${priority}" data-md5="${hash}" data-name="${cName}" data-ext="${cExt}" class="${cEnableClass}"><h1>${cName}</h1><div class="toggle"><div class="inner"><span>on</span><span class="nob"></span><span>off</span></div></div><div class="trash"></div><div class="priority-toggle"></div><div class="pen"></div></li>`;
                    $(".modlist").append($(html));
                    lib.sortModList();

                    //send the callback
                    if (typeof callback === "function") { callback(null, hash); }
                }
            }
        });
    };

    lib.subSortModList = function() {
        const listitems = $('.modlist li[data-priority="1"]');
        listitems.sort(function(a,b) {
            return $(a).find('h1').text().toLowerCase().localeCompare($(b).find('h1').text().toLowerCase());
        });
        $.each(listitems, function(idx, itm) { $(".modlist").append(itm); });

        const listitems2 = $('.modlist li[data-priority="2"]');
        listitems2.sort(function(a,b) {
            return $(a).find('h1').text().toLowerCase().localeCompare($(b).find('h1').text().toLowerCase());
        });
        $.each(listitems2, function(idx, itm) { $(".modlist").append(itm); });
    };

    lib.sortModList = function() {
        var listitems = $(".modlist").children('li').get();
        listitems.sort(function(a, b) {
            return $(a).attr('data-priority').localeCompare($(b).attr('data-priority'));
        });
        $.each(listitems, function(idx, itm) { $(".modlist").append(itm); });
        lib.subSortModList();
    };

    lib.copyPreload = function() {
        const source = `./resources/preload`;
        const target = lib.dir.mod;

        fs.readdir(source, function(err, items) {
            if (!err && items && items.length > 0) {
                items.forEach(function(item) {
                    let found = false;
                    try {
                        const stats = fs.lstatSync(`${target}/${item}`);
                        if (stats) { found = true; }
                    } catch (e) {}

                    if (!found) {
                        fs.createReadStream(`${source}/${item}`).pipe(fs.createWriteStream(`${target}/${item}`));
                    }
                });
            }
        });
    };

    lib.updateManifest = function() {
        const manifestLocation = `${lib.dir.localAppData}\\StreetFighterV\\Saved\\download\\dlc.manifest`;
        let manifest;
        let saveManifest = false;
        try {
            manifest = JSON.parse(fs.readFileSync(manifestLocation, "utf8"));
            if (manifest.is_big_patch) {
                saveManifest = true;
                manifest.is_big_patch = false;
            }

            if (manifest.files.length > 0) {
                saveManifest = true;
                manifest.files = [];
            }

            if (saveManifest) {
                fs.writeFileSync(manifestLocation, JSON.stringify(manifest,null,2));
            }
        } catch (e) {
            window.alert(e);
        }
    };

    lib.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    lib.formatBytes = function(bytes,decimals) {
        if (bytes === 0) {
            return '0 Byte';
        }
        const k = 1000;
        const dm = decimals + 1 || 3;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };

    lib.syncGamePatches = function(callback) {
        const content = [];
        let requiredSpace = 0;
        const downloadPath = `${lib.dir.localAppData}\\StreetFighterV\\Saved\\download`;
        fs.readdir(downloadPath, function(err, items) {
            //first we copy all our dlc content into an array for digestion
            let copied = 0;
            items.forEach(function(item) {
                if (item.endsWith('.pak')) {
                    requiredSpace = requiredSpace + fs.lstatSync(`${lib.dir.localAppData}\\StreetFighterV\\Saved\\download\\${item}`).size;
                    content.push(item);
                }
            });
            const count = content.length;

            //purge paks that aren't part of the dlc
            fs.readdir(`${lib.dir.pak}`, function(err, items) {
                items.forEach(function(item) {
                    if (item.toLowerCase().endsWith('_steam.pak')) {
                        if (content.indexOf(item) < 0) {
                            fs.unlink(`${lib.dir.pak}\\${item}`);
                        }
                    }
                });

                //copy over steam paks
                if (count > 0) {
                    content.forEach(function(item) {
                        if (item.toLowerCase().endsWith('_steam.pak')) {
                            let found = false;
                            try {
                                const stats = fs.lstatSync(`${lib.dir.pak}\\${item}`);
                                if (stats) {
                                    //is this the same size as the source file?
                                    const sourceStats = fs.lstatSync(`${lib.dir.localAppData}\\StreetFighterV\\Saved\\download\\${item}`);

                                    if (!sourceStats || sourceStats.size !== stats.size) {
                                        found = false;
                                    } else {
                                        found = true;
                                    }
                                }
                            } catch (e) {}

                            if (!found) {
                                const drive = lib.dir.mod.slice(0, 1);
                                //we need to hide the entire app ui while syncing
                                $('.fullscreen-sync').show();

                                if (false) {
                                    setTimeout(function() {
                                        window.alert(`Not enough space on drive ${drive} to sync game updates.\n\nPlease make room for ${lib.formatBytes(requiredSpace)} and launch mod manager again.`);
                                        remote.getCurrentWindow().close();
                                    }, 1000);
                                } else {
                                    //copy the file to the paks folder
                                    const stream = fs.createReadStream(`${downloadPath}\\${item}`).pipe(fs.createWriteStream(`${lib.dir.pak}\\${item}`));
                                    stream.on('finish', function() {
                                        copied++;
                                        if (copied >= count) {
                                            if (typeof callback === "function") { callback(); }
                                            $('.fullscreen-sync').fadeOut();
                                        }
                                    });

                                    stream.on('error', function(err){
                                        setTimeout(function() {
                                            fs.unlink(`${lib.dir.pak}\\${item}`);
                                            window.alert(`[Game Version Sync Error]:\n\n${err}\n\nAlso ensure you have at least ${lib.formatBytes(requiredSpace)} free on your ${drive}: drive and restart Mod Manager.`);
                                            remote.getCurrentWindow().close();
                                        }, 1000);
                                    });
                                }
                            }
                        }
                    });
                    if (typeof callback === "function") { callback(); }
                } else {
                    if (typeof callback === "function") { callback(); }
                }
            });
        });
    };

    lib.prepareFramework = function(gamePath, callback) {
        //set local app AppData folder
        if (process.env.LOCALAPPDATA) {
            lib.dir.localAppData = process.env.LOCALAPPDATA;
        } else {
            lib.dir.localAppData = remote.app.getPath('appData');
        }

        lib.dir.game = gamePath;
        lib.dir.pak  = `${lib.dir.game}/StreetFighterV/Content/Paks`;
        lib.dir.mod  = `${lib.dir.pak}/~mods`;
        lib.dir.meta = `${lib.dir.mod}/.meta`;
        lib.mkdir(lib.dir.mod);
        lib.cleanOlder();
        lib.copyPreload();

        //grab the manifest so we know what to do
        lib.manifest.pull((err, manifest) => {
            if (!manifest.slipstream_enabled) {
                //we need to commit the manifest
                manifest.files = [];
                lib.manifest.commit((err, newManifest) => {
                    console.log('manifest loaded (needs syncing):');
                    console.log(newManifest);
                    lib.syncGamePatches(() => {
                        if (typeof callback === "function") { callback(err, manifest); }
                    });
                });
            } else {
                //the manifest is already prepped
                console.log('manifest loaded:');
                console.log(manifest);
                if (typeof callback === "function") { callback(err, manifest); }
            }
        });
    };

    lib.readMods = function(callback) {
        fs.readdir(lib.dir.mod, function(err, files) {
            if (err) {
                return;
            } else {
                const mods = {};
                for (let i=0; i<files.length; i++) {
                    if (!files[i].toLowerCase().startsWith("zzz_modready") && (files[i].endsWith(".dlc")||files[i].endsWith(".dlcbak")||files[i].endsWith(".bak") || files[i].endsWith(".pak"))) {
                        const parts = files[i].split(".");
                        const ext   = parts[parts.length - 1];
                        const name  = files[i].slice(0, (ext.length+1)*-1);
                        mods[name]         = {};
                        mods[name].name    = name;
                        mods[name].fName   = `${name}.${ext}`;
                        mods[name].source  = `${lib.dir.mod}\\${files[i]}`.replace(/\//g,'\\');
                        mods[name].ext     = ext;
                        mods[name].enabled = false;
                        if (ext==="dlc"||ext==="pak") {
                            mods[name].enabled = true;
                        }
                    };
                }
                if (typeof callback === 'function') { callback(mods); }
            }
        });
    };

    lib.pruneDlcPaks = function(callback) {
        const fileList = [];
        lib.manifest.contents.files.forEach((file) => {
            fileList.push(file.name);
        });

        const dlPath = `${lib.dir.localAppData}\\StreetFighterV\\Saved\\download`;
        let r = fs.readdir(dlPath, (err,items) => {
            items.forEach((item) => {
                const dlcbak = `${lib.dir.mod}\\${item.slice(0, -4)}.dlcbak`.replace(/\//g,'\\');
                if (item.endsWith('.pak')
                    && !item.toLowerCase().endsWith('_steam.pak')
                    && fileList.indexOf(item) < 0
                ) {
                    //lets make sure it's not a dlcbak file before we delete it
                    let stat = false;
                    try {
                        stat = fs.lstatSync(dlcbak);
                    } catch (err) { }

                    if (!stat) {
                        fs.unlink(`${dlPath}\\${item}`);
                    }
                }
            });
        });
        callback();
        /*
        //save our registered manifest file list
        const manifestFiles = [];
        lib.manifest.contents.files.forEach((file) => {
            console.log(file.name);
            manifestFiles.push(file.name);
        });

        console.log(manifestFiles);

        //browse the directory
        let stats = false;
        try {
            stats = fs.lstatSync(path);
        } catch (err) {

        }

        if (stats && stats.isDirectory()) {
            fs.readdir(path, function(err, items) {
                items.forEach((item) => {
                    if (!$.inArray(item,manifestFiles) && !item.endsWith('_Steam.pak')) {
                        console.log(`DELETING ${path}\\${item}`);
                        fs.unlink(`${path}\\${item}`);
                    }
                });
            });
            if (typeof callback === "function") { callback(); }
        } else {
            if (typeof callback === "function") { callback(); }
        }
        */
    };

    lib.ensureSingleDlcPak = function(dlcpak, save, callback) {
        const source   = `${lib.dir.mod}\\${dlcpak.name}`.replace(/\//g,'\\');

        if (dlcpak.name.endsWith('.dlc')||dlcpak.name.endsWith('.dlcbak')) {
            //copy the file to the folder
            const active = dlcpak.name.endsWith('.dlc') ? true : false;
            const saveName = active ? dlcpak.name.slice(0,dlcpak.name.length-4) + '.pak' : dlcpak.name.slice(0,dlcpak.name.length-7) + '.pak';
            const target   = `${lib.dir.localAppData}\\StreetFighterV\\Saved\\download\\${saveName}`;
            dlcpak.name = saveName;
            //copy the file to the download directory
            let r, w;
            r = fs.createReadStream(source).pipe(w = fs.createWriteStream(target));
            w.on('finish',() => {
                if (active) { lib.manifest.contents.files.push(dlcpak); }
                if (typeof callback === "function") { callback(); }
            });
            w.on('error',(error) => {
                if (typeof callback === "function") { callback(); }
            });
        }
    };

    lib.ensureDlcPaks = function(dlcpaks) {
        lib.manifest.contents.files = [];
        if (dlcpaks.length > 0) {
            $('.load-indicator .bar .fill').css('width', "0%").css('background-color','#E72DB1');
            $('.load-indicator .bar').find('.text').text('Slipstreaming');
            $('.load-indicator .bar').find('.label').text(`0/${dlcpaks.length}`);
            $('.load-indicator').removeClass('loaded').show();

            let processed = 0;
            const _next = (i) => {
                if (i >= dlcpaks.length) {
                    //final iteration
                    console.log(`commiting manifest:`);
                    console.log(lib.manifest.contents);
                    lib.manifest.commit(() => {
                        lib.pruneDlcPaks(() => {
                            $(".load-indicator").addClass("loaded").fadeOut();
                        });
                    });
                } else {
                    //keep going
                    processed++;
                    const percent = ((processed/dlcpaks.length) * 100).toFixed(0);
                    $('.load-indicator .bar .fill').css('width',`${percent}%`);
                    $('.load-indicator .label').text(`${processed} / ${dlcpaks.length}`);

                    lib.ensureSingleDlcPak(dlcpaks[i], false, () => {
                        _next(processed);
                    });
                }
            };
            _next(0);
        } else {
            //clear manifest
            console.log(`commiting manifest:`);
            console.log(lib.manifest.contents);
            lib.manifest.commit(() => {
                lib.pruneDlcPaks(() => {
                    $(".load-indicator").addClass("loaded").fadeOut();
                });
            });
        }
    };

    lib.populateMods = function(path) {
        lib.readMods((mods) => {
            if (Object.keys(mods).length > 0) {
                let processed = 0;

                $('.load-indicator .bar .fill').css('width', "0%").css('background-color','#4EAAEB');
                $('.load-indicator .bar').find('.text').text('Loading Packages');
                $('.load-indicator').removeClass('loaded').show();
                $(".modlist").html('');

                const dlcpaks = [];
                Object.keys(mods).forEach((key) => {
                    //const source = mods[key].enabled ? `${lib.dir.mod}/${mods[key].name}.pak` : `${lib.dir.mod}/${mods[key].name}.bak`;
                    const mod = {
                        source:mods[key].source,
                        name:mods[key].fName,
                        ext:mods[key].ext,
                        noIO:true
                    };

                    lib.manifest.contents.files = [];
                    lib.addMod(mod, (err, hash) => {
                        processed++;
                        if (mod.ext==="dlc"||mod.ext==="dlcbak") {
                            dlcpaks.push({
                                name: mod.name,
                                hash: hash,
                            });
                        }
                        const percent = ((processed/Object.keys(mods).length) * 100).toFixed(0);
                        $('.load-indicator .bar .fill').css('width',`${percent}%`);
                        $('.load-indicator .label').text(`${processed} / ${Object.keys(mods).length}`);
                        if (processed >= Object.keys(mods).length) {
                            lib.ensureDlcPaks(dlcpaks);
                        }
                    });
                });
            } else {
                lib.ensureDlcPaks([]);
                $(".load-indicator").addClass("loaded").hide();
            }
        });
    };

    lib.togglePriority = function(priority, name, ext, callback) {
        let newExt;

        const newPriority = priority === 1 ? 2 : 1;
        if      (ext === "pak"   ) { newExt = "dlc";    }
        else if (ext === "bak"   ) { newExt = "dlcbak"; }
        else if (ext === "dlc"   ) { newExt = "pak";    }
        else if (ext === "dlcbak") { newExt = "bak";    }

        const oldName = `${lib.dir.mod}/${name}.${ext}`;
        const newName = `${lib.dir.mod}/${name}.${newExt}`;

        fs.rename(oldName, newName, function(err) {
            if (!err) {
                callback({
                    ext: newExt,
                    priority: newPriority,
                });
            } else {
                console.log(err);
            }
        });
    };

    lib.toggleMod = function(name, ext, callback) {
        let newExt;

        if      (ext === "pak"   ) { newExt = "bak";    }
        else if (ext === "bak"   ) { newExt = "pak";    }
        else if (ext === "dlc"   ) { newExt = "dlcbak"; }
        else if (ext === "dlcbak") { newExt = "dlc";    }

        const oldName = `${lib.dir.mod}/${name}.${ext}`;
        const newName = `${lib.dir.mod}/${name}.${newExt}`

        fs.rename(oldName, newName, function(err) {
            if (!err) {
                callback({
                    ext: newExt,
                });
            } else {
                console.log(err);
            }
        });
    };

    lib.renameMod = function(oldName, newName, ext) {
        newName = newName.trim();
        const $el = $(".modlist").find(`li[data-name="${oldName}"]`);
        const source = `${lib.dir.mod}/${oldName}.${ext}`;
        const target = `${lib.dir.mod}/${newName}.${ext}`;

        fs.rename(source, target, function(err) {
            if (err) {
                $el.find('h1').text(oldName);
            } else {
                $el.find('h1').text(sanitize(newName));
                $el.attr('data-name', sanitize(newName));
                lib.sortModList();
            }
        });
    };

    return lib;
};
